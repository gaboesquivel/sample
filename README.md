#

## Directory Structure
```
├── backend
│   ├── client
│   │   ├── assets                     images, fonts
│   │   ├── bower_components
│   │   ├── common                     services, directives, filters
│   │   ├── layouts                    UI layouts, typically UI-Router Abstracts
│   │   ├── lib
│   │   ├── models                     app data models with JSData
│   │   ├── styles                     
│   │   └── views                      each view contained on it's own directory
│   │       ├── home
│   │       ├── login
│   │       ├── signup
│   │       └── etc ..
│   ├── dist                           directory pushed to heroku servers
│   ├── server
│   │   ├── api                        api resources
│   │   │   ├── address
│   │   │   ├── order
│   │   │   └── user
│   │   ├── auth                       authentication logic
│   │   │   └── local
│   │   └── components                 components and helpers             
│   └── tasks                          gulp tasks
├── docs                               documentation for developers
│   └── api
├── mobile
│   ├── app
│   │   ├── assets                     images, fonts
│   │   ├── bower_components
│   │   ├── common                     services, directives, filters
│   │   ├── layouts                    UI layouts, typically UI-Router Abstracts
│   │   ├── lib
│   │   ├── models                     app data models with JSData
│   │   ├── styles                     
│   │   └── views                      each view contained on it's own directory
│   │       ├── home
│   │       ├── login
│   │       ├── signup
│   │       └── etc ..
│   ├── gulp                           gulp tasks
│   ├── heroku                         node server for mobile web version in heroku
│   ├── hooks
│   ├── platforms
│   ├── plugins
│   ├── res
│   └── www                            distribution pushed to ionic view and packed for app stores
└── scripts                            deployment and automation scripts
```

## Configuration for Local Development

### Requirements

- [NodeJS](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.org/)

We recommend nvn for managing different versions as well as avn for automatic node version switching.
[read more](http://gaboesquivel.com/blog/2015/automatic-node-dot-js-version-switching/)

* https://github.com/creationix/nvm#installation
* https://github.com/wbyoung/avn

once you have nvm and avn setup, install the required node version with nvm :
```
nvm install v4.2.4
```

##### Install MongoDB in Mac Os using Homebrew
For [Homebrew](http://brew.sh/), use the following mongodb formula.

* First of, you will need to install Homebrew
```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
* Then install mongodb
```
$ brew install mongodb
```

### Setup the mobile app
```
cd ./mobile  #avn should automatically change node version
npm install
bower install
npm run restore
```

__Android and iOS SDKs__
Follow the Cordova platform guides for [Android](http://cordova.apache.org/docs/en/3.4.0/guide/platforms/android/index.html#Android%20Platform%20Guide) and [iOS](http://cordova.apache.org/docs/en/3.4.0/guide/platforms/ios/index.html#iOS%20Platform%20Guide) to make sure you have everything needed for development on those platforms

__on Mac__
```
brew install android-sdk
```
add `export ANDROID_HOME=/usr/local/opt/android-sdk` to your .bashrc or .zshrc

Open the SDK manager by running: `/usr/local/Cellar/android-sdk/24.4/bin/android`  
You will require:   
1. "SDK Platform" for android-22 Android 5.1.1   
2. "Android SDK Platform-tools (latest)   
3. "Android SDK Build-tools" (latest)]


### Running the Mobile App

##### Web Version

```
gulp watch
```
An instance of the application is running in local environment over the port 8080, to see it open your web browser and go to [http://localhost:8080/](localhost:8080/)

To connect to a remote server from you local instance set the remote server api url in `mobile/app/constants.json`

In order to emulate the web version of the app running in heroku use the following command:
```
npm run heroku-local
```

##### Hybrid App

to open the app in emulator run
```
ionic emulate ios/android [options]
```

to open the app in a connected devices [ typically usb ]
```
ionic run ios/android [options]
```

[read more](http://ionicframework.com/docs/cli/run.html)

### Setup the API server and Admin Dashboard

```
cd ./backend  #avn should automatically change the node version
npm install
bower install
```

We use [motdotla/dotenv](https://github.com/motdotla/dotenv) for environment configurations
This is were we store configuration info we don't on the git the repo.
Rename the sample files to .env and ask your partner for actual values.
```
cp .env-sample .env
cp server/config/.env-sample server/config/.env
```

__!important:__ if SEED_DB=true server/seed.js will run, overriding your database and seeding with sample data.

### Running the Backend

Verify mongoDB is running
```
check with either:
   ps -edaf | grep mongo   # "ps" flags may differ on your OS
or
   /etc/init.d/mongod status
or
   service mongod status
```
note: Windows Path could be: C:Program Files\MongoDB\Server\3.0\bin\mongod

#start the server
```
gulp serve
```

Dashboard will be running in localhost:3000
API will be running in localhost:9000

To run in production mode:
```
gulp preview
```

## Coding Standards and Conventions

Follow [John Papa Styleguide](https://github.com/johnpapa/angular-styleguide) standards in naming and conventions listed.

### linting the mobile app
`npm run lint`


## Deployment

We use continuous integration and deployment.
Circle CI as integration server. https://circleci.com/gh/18techs/closet
If the test suite passes and the applications are built successfully the CI servers deploys the backend and web version to heroku and updates to app in the ionic framework server for testing on real devices.
