'use strict';

var fs = require('fs');
var chalk = require('chalk');
var envFilePath = __dirname.concat('/.env');
if(process.env.NODE_ENV === 'development'){
  console.log(chalk.green('gulpfile.js loading env variables from ' + envFilePath));
  require('dotenv').load({path: envFilePath});
}

var gulp = require('gulp');
var minimist = require('minimist');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var options = gulp.options = minimist(process.argv.slice(2));
options.env = options.env || process.env.NODE_ENV;

gulp.task('default',    ['serve']);
gulp.task('nodemon',    ['watch'],      require('./tasks/serve').nodemon);
gulp.task('serve',      ['nodemon'],    require('./tasks/serve').bsync);
gulp.task('watch',      ['inject'],     require('./tasks/watch'));
gulp.task('inject',     ['env','sass'], require('./tasks/inject'));
gulp.task('sass',                     require('./tasks/sass'));
gulp.task('preview',    ['build'],    require('./tasks/preview'));
gulp.task('build',                    require('./tasks/build'));
gulp.task('env',                      require('./tasks/env'));
gulp.task('bump',       ['version'],  require('./tasks/chore').bump);
gulp.task('version',                  require('./tasks/chore').version);
