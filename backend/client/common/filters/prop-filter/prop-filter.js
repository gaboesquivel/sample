(function () {
  'use strict';

  angular
    .module('dashboardApp')
    .filter('propFilter', propFilter);

    function propFilter () {
      return function (input, prop, filter) {

        if (!filter || filter === 'all') {
          return input;
        }

        return input.filter(function (item) {
          return item[prop] === filter;
        });
      };
    };
})();
