(function() {
  'use strict';

  angular
    .module('dashboardApp')
    .factory('authInterceptor', authInterceptor);

  /* @ngInject */
  function authInterceptor($q, $injector) {
    return {
      // Intercept 401s and redirect you to login if not authenticated
      responseError: function(response) {

        var security = $injector.get('security');
        $log.info('authInterceptor :: responseError', response.status, response, security.isAuthenticated());
        if (response.status === 401 || response.status === 403) {
          security.logout();
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      },
      //identify as dashboard app
      request: function (config) {
         config.headers['client'] = 'dashboard';
         return config;
       }
    };
  }
})();
