(function() {
  'use strict';

  angular
    .module('dashboardApp')
    .factory('utils', utils);

  utils.$inject = ['moment'];

  /* @ngInject */
  function utils(moment) {
    var service = {
      formattedAddressFromEasypostObject: formattedAddressFromEasypostObject,
      formatHour: formatHour,
      addWeekdays: addWeekdays,
      subtractDeliveryDays: subtractDeliveryDays,
      datePickerformatDate: datePickerformatDate,
      datePickerParseDate: datePickerParseDate

    };
    return service;

    function capitalize(s) {
      return s.toLowerCase().replace(/\b./g, function(a) {
        return a.toUpperCase();
      });
    }

    function formattedAddressFromEasypostObject(easypost) {
      var res = '';
      if (easypost.street1) {
        res = res + easypost.street1 + ', ';
      }
      if (easypost.street2) {
        res = res + easypost.street2 + ', ';
      }
      if (easypost.city) {
        res = res + easypost.city + ', ';
      }
      if (easypost.state) {
        res = res + easypost.state + ' ';
      }
      if (easypost.zip) {
        res = res + easypost.zip;
      }
      return capitalize(res);
    }

    function formatHour(n) {
      if (n < 12) {
        return n + ' AM';
      }
      if (n === 12) {
        return '12 PM';
      }
      if (n === 24) {
        return '12 AM';
      } else {
        return n - 12 + ' PM';
      }
    }

    function addWeekdays(date, days) {
      date = moment(date);
      while (days > 0) {
        date = date.add(1, 'days');
        if (date.isoWeekday() !== 7) {
          days -= 1;
        }
      }
      return date;
    }

    function subtractDeliveryDays(date, days) {
      date = moment(date);
      while (days > 0) {
        date = date.subtract(1, 'days');
        if (date.isoWeekday() !== 7) {
          days -= 1;
        }
      }
      return date;
    }

    function datePickerformatDate(date) {
      function pad(n) {
        return n < 10 ? '0' + n : n;
      }

      return date && date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate());
    }

    function datePickerParseDate(s) {
      var tokens = /^(\d{4})-(\d{2})-(\d{2})$/.exec(s);

      return tokens && new Date(tokens[1], tokens[2] - 1, tokens[3]);
    }
  }

})();
