(function() {
  'use strict';

  angular
    .module('dashboardApp')
    .factory('security', security);

  security.$inject = ['$auth', '$state', 'User', 'closetAPI', '$q'];

  /* @ngInject */
  function security($auth, $state, User, closetAPI, $q) {

    var service = {
      login: login,
      logout: logout,
      isAuthenticated: $auth.isAuthenticated,
      changePassword: changePassword,
      resetPassword: resetPassword,
      getLoggedUser: getLoggedUser,
      signup: signup,
      create: create
    };

    var loggedUser;

    return service;

    function login(params) {
      return $auth
        .login(params)
        .then(function onLogin(response) {
          $log.info('security service :: login :: response', response);

          return User
            .me()
            .then(storeLoggedUser);
        });
    }

    function logout() {
      $auth.logout();
      $state.go('public.login');
    }

    function signup(userData) {
      return closetAPI
        .post('/auth/signup', userData)
        .then(function onSignup(response) {
          $auth.setToken(response.token);
          storeLoggedUser(response.user);
        });
    }

    function storeLoggedUser(user) {
      return loggedUser = User.inject(user);
    }

    function changePassword() {

    }

    function resetPassword(user) {
      return closetAPI
        .post('/users/resetpassword', user);
    }

    function getLoggedUser() {

      return $q.when(loggedUser || User.me())
        .then(storeLoggedUser);
    }

    function create(data){
      return closetAPI
        .post('/users/', data);
    }

  }
})();
