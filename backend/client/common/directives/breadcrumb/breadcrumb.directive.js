(function() {
    'use strict';

    angular
        .module('dashboardApp')
        .directive('breadcrumb', breadcrumb);

    /* @ngInject */
    function breadcrumb () {
      var directive = {
          restrict: 'EA',
          templateUrl: 'common/directives/breadcrumb/breadcrumb.html',
          controller: controller,
          controllerAs: 'breadcrumb'
      };
      return directive;

      /* @ngInject */
      function controller ($rootScope, $state) {
        var vm = this;
        vm.states = [];

        activate();

        function activate () {
          $rootScope.$on('$stateChangeSuccess', function () {
            vm.states = [];
            addState($state.$current)
          });

          addState($state.$current);
        };

        function addState (state) {
          vm.states.unshift({ url: state.toString(), name: state.data.name, active: $state.$current.toString() === state.toString() });

          if (state.parent.data) {
            addState(state.parent);
          }
        };
      };
    }
})();
