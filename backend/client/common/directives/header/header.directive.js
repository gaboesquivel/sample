(function() {
    'use strict';

    angular
        .module('dashboardApp')
        .directive('header', header);

    /* @ngInject */
    function header (security, $state, $rootScope) {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: 'common/directives/header/header.html',
            controller: headerCtrl,
            controllerAs: 'vm'
        };
        return directive;

        function link(scope, element, attrs) {

        }

        function headerCtrl(){
          var vm = this;

          vm.logout = logout;
          vm.toggleNav = toggleNav;

          activate();

          function activate() {
            security
              .getLoggedUser()
              .then(function gotUser(user) {
                vm.user = user;
                vm.admin = (user.role === 'admin');
              });
          }

          function logout(){
            security.logout();
          }

          function toggleNav(){
            $rootScope.$emit('toggleNav');
          }

        }
    }
})();
