(function() {
  'use strict';

  angular
    .module('dashboardApp')
    .directive('navbar', navbar);

  /* @ngInject */
  function navbar($rootScope, security, $state) {

    var directive = {
      link: link,
      restrict: 'EA',
      templateUrl: 'common/directives/navbar/navbar.html',
      controller: navbarCtrl,
      controllerAs: 'vm',
      scope: {}
    };
    return directive;

    function link(scope, element, attrs) {

      $rootScope.$on('toggleNav', function() {
        $('body').toggleClass('mini-navbar');
      });

      $('#metisMenu').metisMenu();
    }

    function navbarCtrl ($timeout) {
      var vm = this;
      vm.logout = logout;

      activate();

      function activate() {
        security
          .getLoggedUser()
          .then(function gotUser(user) {
            vm.user = user;
            vm.admin = (user.role === 'admin');
          });
      }

      function logout() {
        security.logout();
      }
    }
  }
})();
