(function() {
    'use strict';

    angular
        .module('dashboardApp')
        .directive('footer', footer);

    /* @ngInject */
    function footer ($state, $rootScope) {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: 'common/directives/footer/footer.html'
        };
        return directive;

        function link(scope, element, attrs) {

        }
    }
})();
