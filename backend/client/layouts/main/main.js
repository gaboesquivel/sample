(function () {
  'use strict';

  angular
    .module('dashboardApp')
    .config(mainLayoutConfig);

    /* @ngInject */
    function mainLayoutConfig ($stateProvider, $templateFactoryProvider) {
      $stateProvider
        .state('main', {
          url: '^',
          templateUrl: 'layouts/main/main.html',
          // /* @ngInject */
          // templateProvider: function ($templateFactory) {
          //   $log.log('main', $templateFactory.fromUrl('layouts/main/main.html'));
          //   return $templateFactory.fromUrl('layouts/main/main.html');
          // },
          data: { name: 'Home', pageTitle: '', bodyClass: '' },
          resolve: {
            loggedUser: ['security', function(security) {
              $log.log('resolve loggedUser');
              return security.getLoggedUser();
            }]
          }
        });
    };
})();
