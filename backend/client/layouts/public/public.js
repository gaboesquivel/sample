(function() {
  'use strict';

  angular
    .module('dashboardApp')
    .config(publicLayoutConfig);

  /* @ngInject */
  function publicLayoutConfig($stateProvider) {
    $stateProvider
      .state('public', {
        url: '^',
        abstract: true,
        template: '<ui-view/>',
        resolve: {
        },
        data: {
        }
      });
  }
})();
