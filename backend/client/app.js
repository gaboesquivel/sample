'use strict';

angular
 .module('dashboardApp', [
   'ngConstants',
   'models',
   'satellizer',
   'ui.router',
   'ui.bootstrap',
   'ui.select',
   'ngCookies',
   'ngSanitize',
   'ngAnimate',
   'ngStorage',
   'ngMessages',
   'ui.mask',
   'ngMask',
   'smart-table',
   'angularMoment',
   'ngMap'
 ])
 .config(appConfig)
 .run(appRun);

 /* @ngInject */
 function appConfig ($stateProvider, $locationProvider, $httpProvider, $urlRouterProvider, $authProvider, API) {
   $urlRouterProvider.otherwise('/');
   $locationProvider.html5Mode(true);
   $httpProvider.interceptors.push('authInterceptor');
   $authProvider.baseUrl = API;
   $authProvider.loginUrl = '/auth/local';
 };

 /* @ngInject */
 function appRun ($rootScope, $state, $log, security, DEBUG, moment) {
   $rootScope.$state = $state;

   // global ref for debugging convinience
   var noop = angular.noop;
   var mutedLogger = {
     log: noop,
     debug: noop,
     error: noop,
     info: noop,
     warn: noop
   };
   window.$log = (DEBUG) ? $log : mutedLogger;

   $rootScope.$on('$stateChangeStart', function(event, next) {
     if (security.isAuthenticated()) {
       if (next.name.indexOf('public.') !== -1) {
         event.preventDefault();
         $state.go('main.dashboard');
       }
     } else {
       if (next.name.indexOf('public.') === -1) {
         event.preventDefault();
         $state.go('public.login');
       }
     }
   });

   moment.locale('en', {
     calendar: {
       lastDay: '[Yesterday]',
       sameDay: '[Today]',
       nextDay: '[Tomorrow]',
       lastWeek: '[last] dddd',
       nextWeek: 'dddd',
       sameElse: 'L'
     }
   });
 };
