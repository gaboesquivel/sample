(function() {
  'use strict';

  angular
    .module('models.user', [])
    .factory('User', UserModelFactory);

  /* @ngInject */
  function UserModelFactory(DS) {

    var UserModel = DS.defineResource({
      name: 'user',
      endpoint: 'users',
      methods: {
        // resource-specific instance methods
      },
      actions: {
        password: {
          method: 'PUT'
        },
        me: {
          method: 'GET',
          response: function(response) {
            return UserModel.createInstance(response.data);
          }
        }
      }
    });

    // static methods
    UserModel.resetPassword = function resetPassword() {
      console.log('UserModel.resetPassword');
    };

    UserModel.getCurrentUser = function getCurrentUser() {
      console.log('UserModel.getCurrentUser');
      return this.me();
    };


    return UserModel;
  }
})();
