(function() {
  'use strict';

  angular
    .module('models', [
      'js-data',
      'models.user'
    ])
    .config(function closetModelsConfig(DSProvider, DSHttpAdapterProvider, API) {
      angular.extend(DSProvider.defaults, { idAttribute: '_id', basePath: API });
      angular.extend(DSHttpAdapterProvider.defaults, { });
    });
})();
