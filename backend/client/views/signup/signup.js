'use strict';

angular.module('dashboardApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('public.signup', {
        url: '/signup',
        templateUrl: 'views/signup/signup.html',
        controller: 'SignupCtrl',
        controllerAs: 'vm',
        data: { pageTitle: '', bodyClass: 'gray-bg' }
      });
  });
