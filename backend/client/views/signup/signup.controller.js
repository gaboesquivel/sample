'use strict';

angular.module('dashboardApp')
  .controller('SignupCtrl', function ($location, security, $state) {

    var vm = this;

    activate();
    function activate(){
      return $state.go('public.login');
    }

    angular.extend(vm, {

      name: 'SignupCtrl',

      signup: function () {

        function updateErrorMessage(msg) {
          $scope.errorMessage = msg || 'Please check your data';
          $scope.SignupFrm.$invalidUser = {
            fail: true
          };
        }

        security.signup(vm.user)
          .then(function () {
            $location.path('/');
            $scope.SignupFrm.$invalidUser = {};
          })
          .catch(function (err) {
            vm.error = err;
            if (err.data) {
              updateErrorMessage(err.data.msg);
            }
          });
      }

    });

  });
