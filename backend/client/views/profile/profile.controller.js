(function() {
    'use strict';

    angular
        .module('dashboardApp')
        .controller('ProfileCtrl', ProfileCtrl);

    /* @ngInject */
    function ProfileCtrl(security, User) {
        /*jshint validthis: true */
        var vm = this;
        vm.name = 'ProfileCtrl';
        vm.updateProfile = updateProfile;

        activate();

        function activate() {
            security
            .getLoggedUser()
            .then(function gotUser(user) {
                vm.user = user;
                vm.admin = (user.role === 'admin');
            });
        }

        function updateProfile(){
            User.update(vm.user._id, vm.user)
                .then(function (doc){
                    if(doc){
                        activate();
                        vm.successUpdate = true;
                    }
                }).catch(function (err){
                    vm.errorUpdate = true;
                });
        }
    }
})();
