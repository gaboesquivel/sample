(function(){
	'use strict';

	angular
    .module('dashboardApp')
    .config(appConfig);

    /* @ngInject */
    function appConfig ($stateProvider) {
      $stateProvider
        .state('public.resetPassword', {
          url: '/resetPassword',
          templateUrl: 'views/resetPassword/resetPassword.html',
          controller: 'ResetPasswordCtrl',
          controllerAs: 'resetPasswordCtrl',
          data: { pageTitle: '', bodyClass: 'gray-bg' }
        });
    }
})();