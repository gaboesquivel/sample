(function() {
  'use strict';

  angular
    .module('dashboardApp')
    .controller('ResetPasswordCtrl', ResetPasswordCtrl);

  ResetPasswordCtrl.$inject = ['$state', 'security'];

  /* @ngInject */
  function ResetPasswordCtrl($state, security) {
    var vm = this;
    vm.send = send;
    vm.showMessage = false;

    activate();

    function activate() {
      
    }

    function send() {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      var check = reg.test(vm.user.email);
      if (check) {
        var user = {
          email: vm.user.email
        };

        security
          .resetPassword(user)
          .then(function(response) {
            if (response.confirm) {
              vm.showMessage = true;
              $('#formReset').fadeOut(500);
            }
          }).catch(function err() {
            vm.errorMessage = true;
          });
      }
    }

  }
})();
