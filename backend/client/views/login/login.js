(function () {
  'use strict';

  angular
    .module('dashboardApp')
    .config(appConfig);

    /* @ngInject */
    function appConfig ($stateProvider) {
      $stateProvider
        .state('public.login', {
          url: '/login',
          templateUrl: 'views/login/login.html',
          controller: 'LoginCtrl',
          controllerAs: 'login',
          data: { pageTitle: '', bodyClass: 'gray-bg' }
        });
    }
})();
