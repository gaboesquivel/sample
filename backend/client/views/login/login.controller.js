(function () {
  'use strict';

  angular
    .module('dashboardApp')
    .controller('LoginCtrl', LoginCtrl);

  /* @ngInject */
  function LoginCtrl ($location, security, $scope) {

    var vm = this;
    vm.login = login;

    activate();

    function activate () {

    }

    function updateErrorMessage(msg) {
      $scope.errorMessage = msg || 'Invalid username or password';
      $scope.LoginFrm.$invalidUser = {
        fail: true
      };
    }

    function login () {
      security
        .login(vm.user)
        .then(function (loggedUser) {
          $scope.LoginFrm.$invalidUser = {};
          if(loggedUser.role !== 'admin'){
            $location.path('/orders');
          }else{
            $location.path('/');
          }
        })
        .catch(function (err) {
          vm.error = err;
          if (err.data) {
            updateErrorMessage(err.data.msg);
          }
        });
    }

  }
})();
