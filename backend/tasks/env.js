'use strict';

/**
 * generates ngConstants file
 */


var gulp = require('gulp');
var ngConstant = require('gulp-ng-constant');
var fs = require('fs');

module.exports = function(){
    var myConfig = JSON.parse(fs.readFileSync('./client/constants.json'));
    var envConfig = myConfig[gulp.options.env];
    return ngConstant({
      templatePath: './client/constants.tpl.ejs',
      constants: envConfig,
      stream: true
    }).pipe(gulp.dest('./client'));
}
