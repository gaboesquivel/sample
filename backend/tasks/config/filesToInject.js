/**
 * Files injected into index.html by gulp-inject
 * used by tasks inject & watch
 */

module.exports = [
  'client/app.js',
  'client/common/**/*.js', '!client/common/**/*.spec.js',
  'client/models/**/*.js', '!client/models/**/*.spec.js',
  'client/views/**/*.js', '!client/views/**/*.spec.js', '!client/views/**/*.e2e.js',
  'client/layouts/**/*.js', '!client/layouts/**/*.spec.js', '!client/layouts/**/*.e2e.js'
];
