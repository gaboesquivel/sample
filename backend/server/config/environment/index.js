'use strict';

var path = require('path');
var _ = require('lodash');

var all = {

  env: process.env.NODE_ENV || 'development',

  ip: process.env.IP || null,

  root: path.normalize(__dirname + '/../../..'),

  port: process.env.PORT || 9000,

  seedDB: process.env.SEED_DB || false,

  mongo: {
    uri: process.env.MONGO_URL || process.env.MONGOLAB_URL || process.env.MONGOLAB_URI || 'mongodb://localhost/closet-dev',
    options: {
      db: {
        safe: true
      }
    }
  },

  secrets: {
    session: process.env.SESSION_SECRET || 'secretKey'
  },

  facebook: {
    clientID:     process.env.FACEBOOK_ID || 'id',
    clientSecret: process.env.FACEBOOK_SECRET || 'secret',
    callbackURL:  (process.env.DOMAIN || '') + '/auth/facebook/callback'
  },

  twitter: {
    clientID:     process.env.TWITTER_ID || 'id',
    clientSecret: process.env.TWITTER_SECRET || 'secret',
    callbackURL:  (process.env.DOMAIN || '') + '/auth/twitter/callback'
  },

  google: {
    clientID:     process.env.GOOGLE_ID || 'id',
    clientSecret: process.env.GOOGLE_SECRET || 'secret',
    callbackURL:  (process.env.DOMAIN || '') + '/auth/google/callback'
  },

  stripe: {
    apiKey: process.env.STRIPE_KEY || '',
    stripePubKey: process.env.STRIPE_PUB_KEY || ''
  },

  mailgun: {
    apiKey: process.env.MAILGUN_SECRET || '',
    domain: process.env.MAILGUN_DOMAIN || ''
  }
};

module.exports = _.merge(all, require('./' + all.env + '.js'));
