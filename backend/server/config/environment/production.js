'use strict';

module.exports = {
  // MongoDB connection options
  mongo: {
    options: {
      db: {
        safe: true,
        replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
        server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
      }
    }
  }
};
