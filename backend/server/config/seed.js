/**
 * Populate DB with sample data on server start
 * to disable set `SEED_DB=false` in process environment variables
 * use `config/.env` for development
 */

'use strict';
var _ = require('lodash');
var User = require('../api/user/user.model');
var chalk = require('chalk');
console.log(chalk.yellow('Populating database...'));


User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test',
    phone: '(777) 777-7777'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin',
    phone: '(888) 888-8888'
  }, function(err, testUser, adminUser) {
      console.log('finished populating users');
    }
  );
});
