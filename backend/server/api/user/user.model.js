'use strict';

var crypto = require('crypto'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  timestamps = require('mongoose-timestamp'),
  Q = require('q'),
  config = require('../../config/environment'),
  stripe = require('stripe')(config.stripe.apiKey);

var UserSchema = new Schema({
  name: String,
  phone: String,
  cleanerName: String,
  email: {
    type: String,
    lowercase: true
  },
  role: {
    type: String,
    default: 'user'
  },
  stripeCustomer: {},
  resetPass: false,
  passwordHash: {
    type: String,
    select: false
  },
  salt: {
    type: String,
    select: false
  }
});

/**
 * Virtuals
 */

UserSchema
  .virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.passwordHash = this.encryptPassword(password);
  })
  .get(function() {
    return this._password;
  });

/**
 * Validations
 */

UserSchema
  .path('email')
  .validate(function(value, respond) {
    var self = this;
    this.constructor.findOne({
      email: value
    }, function(err, user) {
      if (err) {
        throw err;
      }
      if (user) {
        if (self.id === user.id) {
          return respond(true);
        }
        return respond(false);
      }
      respond(true);
    });
  }, 'email already used');

/**
 * Methods
 */

UserSchema.methods.authenticate = function(password) {
  return this.encryptPassword(password) === this.passwordHash;
};

UserSchema.methods.makeSalt = function() {
  return crypto.randomBytes(16).toString('base64');
};

UserSchema.methods.encryptPassword = function(password) {
  if (!password || !this.salt) {
    return '';
  }
  var salt = new Buffer(this.salt, 'base64');
  return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
};


UserSchema.methods.addCard = function addCard(stripeToken) {
  console.log('User :: addCard', this);
  var user = this;

  function updateUserStripeCostumerData(customer){
    user.stripeCustomer = customer;
    user.markModified('stripeCustomer');
    return user.save();
  }

  if (!user.stripeCustomer) {
    console.log('User :: addCard :: !customerId');

    return stripe.customers.create({
      description: 'Customer for ' + user.email,
      source: stripeToken.id
    })
    .catch(function (err) {
      console.log('error creating stripe customer', err);
      throw err;
    })
    .then(updateUserStripeCostumerData);

  } else {

    return stripe.customers.createSource(user.stripeCustomer.id, {
      source: stripeToken.id
    })
    .catch(function (err) {
      console.log('error creating stripe source', err);
      throw err;
    })
    .then(function createStripeSourceSuccess (source) {
      console.log('sucessfully created stripe customer', source);
      return stripe.customers.retrieve(user.stripeCustomer.id);
    })
    .catch(function (err) {
      console.log('error retrieving stripe customer', err);
      throw err;
    })
    .then(updateUserStripeCostumerData);
  }
};


UserSchema.methods.setAsDefaultCard = function(card) {
  var deferred = Q.defer();
  var user = this;
  var dataObj = {
    default_source: card.id
  };

  stripe.customers.update(user.stripeCustomer.id, dataObj, function (err, confirmation) {
    if (err) {
      return deferred.reject(err);
    }
    stripe.customers.retrieve(user.stripeCustomer.id,
      function(err, customer) {
        if (err) {
          return deferred.reject(err);
        }
        user.stripeCustomer = customer;
        user.markModified('stripeCustomer');
        user.save()
          .then(function userSaveSuccess(user) {
            deferred.resolve(user);
          })
          .catch(function userSaveError(err) {
            deferred.reject(err);
          });
      }
    );
  });

  return deferred.promise;

};

UserSchema.methods.removeCard = function(card) {
  var deferred = Q.defer();
  var user = this;
  stripe.customers.deleteCard(user.stripeCustomer.id, card.id, function (err, confirmation) {
    if (err) {
      return deferred.reject(err);
    }
    stripe.customers.retrieve(user.stripeCustomer.id,
      function(err, customer) {
        if (err) {
          return deferred.reject(err);
        }
        console.log('customers.retrieve XXXXXX: ', customer);
        user.stripeCustomer = customer;
        user.markModified('stripeCustomer');
        user.save()
          .then(function userSaveSuccess(user) {
            deferred.resolve(user);
          })
          .catch(function userSaveError(err) {
            deferred.reject(err);
          });
      }
    );
  });

  return deferred.promise;

};


UserSchema.plugin(timestamps);
module.exports = mongoose.model('User', UserSchema);
