'use strict';

var _ = require('lodash');

var authService = require('../../auth/auth.service');
var User = require('./user.model');
var generatorPassword = require('generate-password');
var config = require('../../config/environment');
var mailgun = require('mailgun-js')({apiKey: config.mailgun.apiKey, domain: config.mailgun.domain});

function handleError (res, err) {
  return res.status(500).send(err);
}

//Get list of users
exports.index = function(req, res){
  User.find({}, cb);

  function cb(err, users){
    if(err){
      return handleError(res, err);
    }else{
      return res.status(200).json(users);
    }
  }
}

exports.addCard = function (req, res) {

  req.user.addCard(req.body)
    .then(function addCardSuccess(user) {
      return res.status(200).json(user);
    })
    .catch(function addCardError(err) {
      handleError(err, res);
    });
};
exports.removeCard = function (req, res) {
  req.user.removeCard(req.body)
    .then(function addCardSuccess(user) {
      return res.status(200).json(user);
    })
    .catch(function addCardError(err) {
      handleError(err, res);
    });
};
exports.setAsDefaultCard = function (req, res) {
  req.user.setAsDefaultCard(req.body)
    .then(function addCardSuccess(user) {
      return res.status(200).json(user);
    })
    .catch(function addCardError(err) {
      handleError(err, res);
    });
};
exports.create = function (req, res) {
  User.create(req.body, function (err, user) {
    if (err) { return handleError(res, err); }
    res.status(201).json(_.omit(user.toObject(), ['passwordHash', 'salt']));
  });
};


exports.getMe = function (req, res) {
  User.findById(req.user._id, function (err, user) {
    if (err) { return handleError(res, err); }
    if (!user) { return res.json(401); }
    res.status(200).json(user);
  });
};

exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  User.findById(req.params.id, function(err, user) {
    if (err) {
      return handleError(res, err);
    }
    if (!user) {
      return res.status(404).send('Not Found');
    }
    var updated = _.merge(user, req.body);
    updated.save(function(err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(user);
    });
  });
};

exports.changePassword = function(req, res){
  // Change password an user
  User.findById(req.body.id, '+passwordHash +salt', function(err, user){
    if (err) { return handleError(res, err); }
    if(!user) { return res.json(401); }
    if(user){
      if(user.authenticate(req.body.oldPassword)){
        user.set('password', req.body.newPassword);
        user.save(function saving(errSave){
        if(errSave) return handleError(res, errSave);
          res.json({confirm: true});
        });
      }else{
        return res.json(401);
      }
    }
  });
};

exports.resetPassword = function(req, res){
  // Create a temporary password an user
  User.findOne({email: req.body.email}, function (err, user){
    if (err) { return handleError(res, err); }
    if(!user) { return res.json(401); }
    if(user){
      var passwordTemporary  = generatorPassword.generate({
        length: 15,
        numbers: true
      });
      user.set('password', passwordTemporary);
      user.save(function (errSave){
        if(errSave) return handleError(res, errSave);
        var data = {
          from: 'Excited User <support@closet.com>',
          to: user.email,
          subject: "Reset Password",
          text: "your temporary password is: " + passwordTemporary
        };
        mailgun.messages().send(data, function (errsend){
          if(errsend) return handleError(res, errsend);
          res.json({confirm: true});
        });
      });
    }
  });
};
