'use strict';

var express = require('express');
var router = express.Router();
var controller = require('./user.controller');
var auth = require('../../auth/auth.service');

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/me', auth.isAuthenticated(), controller.getMe);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.post('/changePassword', auth.isAuthenticated(), controller.changePassword);
router.post('/resetpassword', controller.resetPassword);
router.post('/:id/cards', auth.isAuthenticated(), controller.addCard);
router.post('/:id/cards/remove', auth.isAuthenticated(), controller.removeCard);
router.post('/:id/cards/setasdefault', auth.isAuthenticated(), controller.setAsDefaultCard);


module.exports = router;
