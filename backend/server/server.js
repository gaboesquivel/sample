'use strict';
var fs = require('fs');
var chalk = require('chalk');
var envFilePath = __dirname.concat('/config/.env');
var cors = require('cors');

if(process.env.NODE_ENV === 'development'){
  console.log(chalk.green('server.js loading env variables from ' + envFilePath));
  require('dotenv').load({path: envFilePath});
}

var express = require('express');
var config = require('./config/environment');
var mongoose = require('mongoose');

mongoose.connect(config.mongo.uri, config.mongo.options);

if(config.seedDB && config.seedDB != 'false') {
  require('./config/seed');
}

var app = express();
app.use(cors());

var server = require('http').createServer(app);
var socket = require('socket.io')(server, { serveClient: true });
require('./config/sockets.js')(socket);

require('./config/express')(app);
require('./routes')(app);

server.listen(config.port, config.ip, function () {

  console.log(
    chalk.red('\nExpress server listening on port ')
    + chalk.yellow('%d')
    + chalk.red(', in ')
    + chalk.yellow('%s')
    + chalk.red(' mode.\n'),
    config.port,
    app.get('env')
  );

  if (config.env === 'development') {
    require('ripe').ready();
    console.log(chalk.green('Running application with the following configuration:'), JSON.stringify(config, null, 2));
  }

});

module.exports = server;
