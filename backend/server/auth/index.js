'use strict';

var express = require('express');
var router = express.Router();
var auth = require('./auth.service');
var User = require('../api/user/user.model');
var _ = require('lodash');

require('./local/passport');

router.use('/local', require('./local'));

router.post('/signup', function (req, res, next) {
  User.create(req.body, function (err, user) {
    if (err) { return res.status(500).send(err); }
    res.status(201).json({
      user: _.omit(user.toObject(), ['passwordHash', 'salt']),
      token: auth.signToken(user._id)
    });
  });
});

module.exports = router;
