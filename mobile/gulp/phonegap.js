'use strict';

var gulp = require('gulp');
// var runSequence = require('run-sequence');
var del = require('del');
var rename = require('gulp-rename');


var PHONEGAP_BUILD_FOLDER = './phonegap',
  PHONEGAP_RAW_FOLDER = '',
  IONIC_SOURCE_FOLDER = './www',
  IONIC_RESOURCE_FOLDER = './resources';

gulp.task('phonegap', ['clean_phonegap',
  'copy_www',
  'copy_resources',
  'copy_phonegap_config_xml',
  'copy_default_icon',
  'copy_default_splash'], function (cb) {
    if (typeof cb === 'function') {
      cb();
    }
  }
);

gulp.task('clean_phonegap', function (cb) {
  // clean our folder first
  var phonegapDelPattern = PHONEGAP_BUILD_FOLDER + '/*';
  del([phonegapDelPattern], {
    force: true
  }, cb);
});

gulp.task('copy_www', function () {
  var targetPhonegapFolder = PHONEGAP_BUILD_FOLDER + '/' + PHONEGAP_RAW_FOLDER;
  return gulp.src([IONIC_SOURCE_FOLDER + '/**'])
    .on('error', swallowError)
    .pipe(gulp.dest(targetPhonegapFolder));
});

gulp.task('copy_phonegap_config_xml', function () {
  var targetPhonegapFolder = PHONEGAP_BUILD_FOLDER + '/' + PHONEGAP_RAW_FOLDER;
  return gulp.src(['./config-phonegap-build.xml'])
    .on('error', swallowError)
    .pipe(rename('config.xml'))
    .pipe(gulp.dest(targetPhonegapFolder));
});

gulp.task('copy_resources', ['copy_www'], function () {
  var targetPhonegapFolder = PHONEGAP_BUILD_FOLDER + '/' + PHONEGAP_RAW_FOLDER + '/resources',
    excludePattern = '!' + IONIC_RESOURCE_FOLDER + '/_source_assets{,/**}';

  return gulp.src([excludePattern, IONIC_RESOURCE_FOLDER + '/**', IONIC_RESOURCE_FOLDER + '/.pgbomit'])
    .on('error', swallowError)
    .pipe(gulp.dest(targetPhonegapFolder));
});

gulp.task('copy_default_icon', function () {
  var targetPhonegapFolder = PHONEGAP_BUILD_FOLDER + '/' + PHONEGAP_RAW_FOLDER;
  return gulp.src([IONIC_RESOURCE_FOLDER + '/icon.png'])
    .on('error', swallowError)
    .pipe(gulp.dest(targetPhonegapFolder));
});

gulp.task('copy_default_splash', function () {
  var targetPhonegapFolder = PHONEGAP_BUILD_FOLDER + '/' + PHONEGAP_RAW_FOLDER;
  return gulp.src([IONIC_RESOURCE_FOLDER + '/splash.png'])
    .on('error', swallowError)
    .pipe(gulp.dest(targetPhonegapFolder));
});

function swallowError (error) {
  //If you want details of the error in the console
  console.log(error.toString());
  this.emit('end');
}
