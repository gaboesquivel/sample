(function() {

  'use strict';

  angular.module('mobileApp', [
    'ngConstants',
    'appModels',
    'ionic',
    'ngCordova',
    'ui.router',
    'uiGmapgoogle-maps',
    'mp.datePicker',
    'angularMoment',
    'ngStorage',
    'ngResource',
    'ngMask',
    'ngMessages',
    'credit-cards',
    'angular-stripe',
    'ui.mask',
    'ionic-ratings',
    'satellizer'
  ])
    .config(mainConfig)
    .run(mainRunBlock);

  function mainConfig($stateProvider, $urlRouterProvider, $logProvider, DEBUG, API, $httpProvider, stripeProvider, STRIPE_KEY, $authProvider, $ionicConfigProvider, moment, $localStorageProvider) {

    $localStorageProvider.setKeyPrefix('Closet-');

    $ionicConfigProvider.views.maxCache(0);

    $logProvider.debugEnabled(DEBUG || false);

    stripeProvider.setPublishableKey(STRIPE_KEY);

    $httpProvider.interceptors.push('authInterceptor');
    $urlRouterProvider.otherwise('/main/home');

    $authProvider.baseUrl = API;
    $authProvider.loginUrl = '/auth/local';

    moment.locale('en', {
      calendar: {
        lastDay: '[Yesterday]',
        sameDay: '[Today]',
        nextDay: '[Tomorrow]',
        lastWeek: '[last] dddd',
        nextWeek: 'dddd',
        sameElse: 'L'
      }
    });
  }

  mainRunBlock.$inject = ['$log', '$rootScope', '$ionicPlatform', '$state', 'security', 'DEBUG'];
  function mainRunBlock($log, $rootScope, $ionicPlatform, $state, security, DEBUG) {

    // global ref for debugging convinience
    var noop = angular.noop;
    var mutedLogger = {
      log: noop,
      debug: noop,
      error: noop,
      info: noop,
      warn: noop
    };
    window.$log = (DEBUG) ? $log : mutedLogger;

    $rootScope.$on('$stateChangeStart', function(event, next) {
      if (security.isAuthenticated()) {
        if (next.name.indexOf('public.') !== -1) {
          event.preventDefault();
          $state.go('main.home');
        }
      } else {
        if (next.name.indexOf('public.') === -1) {
          event.preventDefault();
          $state.go('public.login');
        }
      }
    });
    // fetch data if user is authenticated
    $rootScope.$on('userAuthenticated', onUserAuthentication);
    if (security.isAuthenticated) {
      onUserAuthentication();
    }
    function onUserAuthentication() {
    }

  }

})();
