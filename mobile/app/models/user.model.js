(function() {
  'use strict';

  angular
    .module('appModels.user', [])
    .factory('User', UserModelFactory);

  /* @ngInject */
  function UserModelFactory(DS) {
    var UserModel = DS.defineResource({
      name: 'user',
      endpoint: 'users',
      methods: {
        // resource-specific instance methods
        password: {
          method: 'PUT'
        }
      },
      actions: {
        me: {
          method: 'GET',
          response: function(response) {
            return UserModel.createInstance(response.data);
          }
        }
      },
      defaultValues: {
        stripeCustomer: {
          sources: {
            data: { }
          },
          default_source: null
        }
      }
    });

    // static methods
    UserModel.resetPassword = function resetPassword() {
      console.log('UserModel.resetPassword');
    };

    UserModel.getCurrentUser = function getCurrentUser() {
      console.log('UserModel.getCurrentUser');
      return this.me();
    };


    return UserModel;
  }
})();
