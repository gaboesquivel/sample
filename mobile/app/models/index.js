(function() {
  'use strict';

  angular
    .module('appModels', [
      'js-data',
      'appModels.user'
    ])
    .config(function closetModelsConfig(DSProvider, DSHttpAdapterProvider, API) {
      angular.extend(DSProvider.defaults, { idAttribute: '_id', basePath: API });
      angular.extend(DSHttpAdapterProvider.defaults, { });
    });
})();
