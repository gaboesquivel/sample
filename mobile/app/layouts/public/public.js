(function() {
  'use strict';

  angular.module('mobileApp')
    .config(publicLayoutConfig);

  function publicLayoutConfig($stateProvider) {
    $stateProvider
      .state('public', {
        url: '/',
        abstract: true,
        templateUrl: 'layouts/public/public.html',
        resolve: {
        },
        data: {
        }
      });
  }
})();
