(function() {
  'use strict';

  angular
    .module('mobileApp')
    .controller('MainCtrl', MainCtrl);

  MainCtrl.$inject = ['security', '$rootScope', '$state'];

  /* @ngInject */
  function MainCtrl(security, $rootScope, $state) {
    var vm = this;
    vm.logout = logout;
    vm.enableMenu = true;
    vm.showBack = false;
    vm.backCheckout = backCheckout;

    activate();

    function activate() {
    }

    function logout() {
      security.logout();
    }

    $rootScope.$on('backCheckout', function() {
      vm.showbackbutton = true;
    });

    function backCheckout() {
      vm.showbackbutton = false;
      $state.go('main.orderCheckout');
    }

  }
})();
