(function() {
  'use strict';

  angular.module('mobileApp')
    .config(mainLayoutConfig);

  function mainLayoutConfig($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/main',
        cache: false,
        abstract: true,
        templateUrl: 'layouts/main/main.html',
        controller: 'MainCtrl as mainCtrl',
        resolve: {
          currentUser: ['security', function(security) {
            return security.getLoggedUser();
          }]
        }
      });
  }
})();
