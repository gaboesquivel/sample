(function() {
  'use strict';

  angular
    .module('mobileApp')
    .filter('centsToDollars', centsToDollars);

  function centsToDollars() {
    return centsToDollarsFilter;

    function centsToDollarsFilter(amount) {
      return (amount / 100).toFixed(2);
    }
  }
})();
