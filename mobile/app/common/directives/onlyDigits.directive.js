(function() {
  'use strict';

  angular
    .module('mobileApp')
    .directive('onlyDigits', onlyDigits);

  /* @ngInject */
  function onlyDigits() {
    var directive = {
      restrict: 'A',
      require: '?ngModel',
      link: linkFunc
    };

    return directive;

    function linkFunc(cope, element, attrs, modelCtrl) {

      modelCtrl.$parsers.push(function(inputValue) {
        if (inputValue === undefined) {
          return '';
        }

        var transformedInput = inputValue.replace(/[^0-9]/g, '');
        if (transformedInput !== inputValue) {
          modelCtrl.$setViewValue(transformedInput);
          modelCtrl.$render();
        }
        return transformedInput;
      });
    }

  }
})();
