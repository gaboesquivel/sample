(function() {
  'use strict';

  angular
    .module('mobileApp')
    .factory('appAPI', appAPI);

  appAPI.$inject = ['$q', '$http', 'API', '$log'];
  /* @ngInject */
  function appAPI($q, $http, API, $log) {

    var basePath = API;

    function makeRequest(verb, uri, data) {
      $log.log('appAPI :: ' + verb + ' :: ' + uri, data);
      var defer = $q.defer();
      verb = verb.toLowerCase();

      //start with the uri
      var httpArgs = [basePath + uri];
      if (verb.match(/post|put/)) {
        httpArgs.push(data);
      }

      $http[verb].apply(null, httpArgs)
        .success(function(data) {
          defer.resolve(data);
        })
        .error(function(data, status) {
          defer.reject('HTTP Error: ' + status);
        });

      return defer.promise;
    }

    return {
      get: function(uri) {
        return makeRequest('get', uri);
      },
      post: function(uri, data) {
        return makeRequest('post', uri, data);
      },
      put: function(uri, data) {
        return makeRequest('put', uri, data);
      },
      delete: function(uri) {
        return makeRequest('delete', uri);
      }
    };
  }
})();
