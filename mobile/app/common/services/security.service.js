(function() {
  'use strict';

  angular
    .module('mobileApp')
    .factory('security', security);

  security.$inject = ['$auth', '$state', 'User', 'appAPI', '$q', '$rootScope'];

  /* @ngInject */
  function security($auth, $state, User, appAPI, $q, $rootScope) {

    var service = {
      login: login,
      logout: logout,
      isAuthenticated: $auth.isAuthenticated,
      changePassword: changePassword,
      resetPassword: resetPassword,
      getLoggedUser: getLoggedUser,
      updateUser: updateUser,
      signup: signup
    };

    var loggedUser;

    return service;

    function login(params) {
      return $auth
        .login(params)
        .then(function onLogin(response) {
          $log.info('security service :: login :: response', response);
          $rootScope.$emit('userAuthenticated');
          return User
            .me()
            .then(storeLoggedUser);
        });
    }

    function logout() {
      $auth.logout();
      $rootScope.$emit('logout');
      $state.go('public.login');
    }

    function signup(userData) {
      return appAPI
        .post('/auth/signup', userData)
        .then(function onSignup(response) {
          $auth.setToken(response.token);
          $rootScope.$emit('userAuthenticated');
          storeLoggedUser(response.user);
        });
    }

    function updateUser() {
      return User.me().then(storeLoggedUser);
    }

    function storeLoggedUser(user) {
      return loggedUser = User.inject(user);
    }

    function changePassword() {

    }

    function resetPassword(user) {
      if (user.id && user.newPassword) {
        return appAPI
          .post('/users/changePassword', user);
      }
      if (user.email) {
        return appAPI
          .post('/users/resetpassword', user);
      }
    }

    function getLoggedUser() {

      return $q.when(loggedUser || User.me())
        .then(storeLoggedUser);
    }

  }
})();
