(function() {
  'use strict';

  angular
    .module('mobileApp')
    .factory('geo', geo)
    .run(initializeGeoService);

  geo.$inject = ['$cordovaGeolocation', '$q', '$ionicPopup'];

  /* @ngInject */
  function geo($cordovaGeolocation, $q, $ionicPopup) {

    var currentPosition;
    var posOptions = {
      timeout: 6000,
      enableHighAccuracy: true
    };

    var service = {
      getCurrentPosition: getCurrentPosition
    };

    var positionPromise;

    return service;

    function getCurrentPosition(options) {

      if (positionPromise) {
        return positionPromise;
      }

      if (!_.get(options, 'force') && currentPosition) {
        $log.log('geo service :: getCurrentPosition :: return cached position', currentPosition);
        return positionPromise = $q.when(currentPosition).then(function() {
          positionPromise = null;
          return currentPosition;
        });
      }

      $log.log('geo service :: getCurrentPosition :: get from cordovaGeolocation');
      return positionPromise = $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function(position) {
          $log.log('geo service :: getCurrentPosition :: got location from cordovaGeolocation', position);
          currentPosition = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          return currentPosition;
        })
        .catch(function(error) {
          showLocationAccessErrorMessage();
          $log.error('geo service :: getCurrentPosition error', error);
        })
        .finally(function() {
          positionPromise = null;
        });

    }

    function showLocationAccessErrorMessage() {
      $ionicPopup.alert({
        title: 'Geolocation Error',
        template: 'We are not able to detect your current location, please check your permissions settings.<br><br> Contact support@closet.com if the problem persist.'
      });
    }

  }

  // initializeGeoService
  function initializeGeoService(geo) {
    geo.getCurrentPosition();
  }

})();
