(function() {
  'use strict';

  angular
    .module('mobileApp')
    .factory('authInterceptor', authInterceptor);

  authInterceptor.$inject = ['$q', '$injector'];

  /* @ngInject */
  function authInterceptor($q, $injector) {
    return {
      // Intercept 401s and redirect you to login if not authenticated
      responseError: function(response) {

        var security = $injector.get('security');
        $log.info('authInterceptor :: responseError', response.status, response, security.isAuthenticated());
        if (response.status === 401) {
          security.logout();
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      }
    };
  }
})();
