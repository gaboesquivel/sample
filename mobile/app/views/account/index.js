(function() {
  'use strict';
  angular.module('mobileApp')
    .config(accountConfig);

  function accountConfig($stateProvider) {
    $stateProvider
    .state('main.account', {
      url: '/account',
      views: {
        'pageContent': {
          templateUrl: 'views/account/account.html',
          controller: 'AccountCrtl as accountCrtl'
        }
      }
    })
    .state('main.profile', {
      url: '/account/profile',
      views: {
        'pageContent': {
          templateUrl: 'views/account/profile.html',
          controller: 'AccountCrtl as accountCrtl'
        }
      }
    })
    .state('main.paymentMethods', {
      url: '/account/paymentMethods',
      views: {
        'pageContent': {
          templateUrl: 'views/account/paymentMethods.html',
          controller: 'PaymentMethodsCrtl as paymentMethodsCrtl'
        }
      }
    });
  }
})();
