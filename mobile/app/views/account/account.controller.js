(function() {
  'use strict';

  angular
    .module('mobileApp')
    .controller('AccountCrtl', AccountCrtl);

  AccountCrtl.$inject = ['currentUser', 'User', '$ionicLoading', 'security'];

  /* @ngInject */
  function AccountCrtl(currentUser, User, $ionicLoading, security) {
    var vm = this;
    vm.updateUserProfile = updateUserProfile;

    activate();

    function activate() {
      vm.active = true;
      vm.user = currentUser;
      vm.security = security;
    }

    function updateUserProfile() {
      $ionicLoading.show({
        template: '<ion-spinner class=\'spinner-energized\'></ion-spinner><br>Updating profile...'
      });
      User
        .update(currentUser._id, currentUser)
        .then(function userUpdateSuccess() {
          //TODO: verify currentUser get updated for global use
          vm.editingProfile = false;
        })
        .catch(function userUpdateError(error) {
          $log.error('AccountCrtl :: userUpdateError', error);
        })
        .finally(function() {
          $ionicLoading.hide();
        });
    }
  }
})();
