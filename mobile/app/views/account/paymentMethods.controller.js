(function() {
  'use strict';

  angular
    .module('mobileApp')
    .controller('PaymentMethodsCrtl', PaymentMethodsCrtl);

  PaymentMethodsCrtl.$inject = ['User', '$ionicPopup', '$ionicModal', '$scope', '$ionicLoading', 'stripe', 'appAPI', 'security', '$timeout'];

  /* @ngInject */
  function PaymentMethodsCrtl(User, $ionicPopup, $ionicModal, $scope, $ionicLoading, stripe, appAPI, security, $timeout) {
    var vm = this;
    vm.loading = true;
    vm.cards = [];
    vm.user;
    vm.newCard;

    vm.removeCard = removeCard;
    vm.setAsDefaultCard = setAsDefaultCard;
    vm.showNewCardModal = showNewCardModal;
    vm.cancelAddNewCard = cancelAddNewCard;
    vm.saveNewCard = saveNewCard;

    activate();

    function activate() {

      // get user data
      security
        .getLoggedUser()
        .then(function(user) {
          vm.user = user;
          vm.loading = false;
          vm.cards = user.stripeCustomer.sources.data;
          vm.defaultCardId = user.stripeCustomer.default_source;
        })
        .catch(function(err) {
          $log.error(err);
        });
    }

    function removeCard(card) {
      $ionicPopup.confirm({
        title: 'Default card',
        template: 'Do you want to remove this card?'
      }).then(function(res) {
        // res : boolean
        if (res) {
          if (card && card.id) {
            $ionicLoading.show({
              template: '<ion-spinner class=\'spinner-energized\'></ion-spinner><br>Removing card...'
            });
            appAPI.post('/users/' + vm.user._id + '/cards/remove', card)
              .then(security.updateUser)
              .then(function removeCardSuccess(user) {
                $timeout(function() {
                  $scope.$apply(function() {
                    vm.user = user;
                    vm.cards = user.stripeCustomer.sources.data;
                    vm.defaultCardId = user.stripeCustomer.default_source;
                  });
                });
              })
              .catch(function removeCardError(error) {
                // handle error
                $log.error(error);
              })
              .finally(function removeCardFin() {
                $ionicLoading.hide();
              });
          }
        }
      });
    }

    function setAsDefaultCard(card, index) {
      if (vm.defaultCardId !== card.id) {
        $ionicPopup.confirm({
          title: 'Default card',
          template: 'Do you want make this the default card?'
        }).then(function(res) {
          // res :: boolean
          if (res) {
            if (card && card.id) {
              $ionicLoading.show({
                template: '<ion-spinner class=\'spinner-energized\'></ion-spinner><br>Updating card...'
              });
              appAPI.post('/users/' + vm.user._id + '/cards/setasdefault', card)
                .then(security.updateUser)
                .then(function removeCardSuccess(user) {
                  $timeout(function() {
                    $scope.$apply(function() {
                      vm.user = user;
                      vm.cards = user.stripeCustomer.sources.data;
                      vm.defaultCardId = user.stripeCustomer.default_source;
                    });
                  });

                })
                .catch(function removeCardError(error) {
                  // handle error
                  $log.error(error);
                  $ionicLoading.hide();
                })
                .finally(function removeCardFin() {
                  $ionicLoading.hide();
                });
            }
          }

        });
        $log.log('setAsDefaultCard', card, index);
      }
    }

    function showNewCardModal() {
      $ionicModal
        .fromTemplateUrl('views/account/newPaymentMethod.modal.html', {
          scope: $scope,
          animation: 'slide-in-up',
          focusFirstInput: false
        })
        .then(function(modal) {
          vm.modal = modal;
          vm.modal.show();
          $scope.PageTitle = 'New Card';
          $scope.ButtonLabel = 'Add Card';
        });
    }

    function cancelAddNewCard() {
      vm.modal.hide();
    }

    function saveNewCard(paymentForm) {
      $log.debug(paymentForm);
      if (paymentForm.$invalid) {
        return;
      }
      vm.savingNewCard = true;
      $ionicLoading.show({
        template: '<ion-spinner class=\'spinner-energized\'></ion-spinner><br>Validating card...'
      });

      stripe.card
        .createToken(vm.newCard)
        .catch(function newCardErrorHandler() {
          $log.debug('error creating the token');
        })
        .then(function addUserCard(token) {
          $log.debug('token created for card ending in ', token);
          $ionicLoading.show({
            template: '<ion-spinner class=\'spinner-energized\'></ion-spinner><br>Saving card...'
          });

          var newUser = angular.copy(vm.user);
          newUser.stripeToken = token;

          appAPI.post('/users/' + vm.user._id + '/cards', token)
            .then(security.updateUser)
            .then(function(user) {
              $log.debug('got user', user);
              $timeout(function() {
                $scope.$apply(function() {
                  vm.user = user;
                  vm.cards = user.stripeCustomer.sources.data;
                  vm.defaultCardId = user.stripeCustomer.default_source;
                  vm.newCard = {};
                  paymentForm.$setPristine();
                  vm.allSet = true;
                });
              });
            })
            .catch(function addCardError(error) {
              // handle error
              $log.error(error);
            })
            .finally(function addCardFin() {
              $ionicLoading.hide();
              vm.modal.hide();
              vm.savingNewCard = false;
            });
        });
    }
  }
})();
