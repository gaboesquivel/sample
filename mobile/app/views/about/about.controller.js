(function() {
  'use strict';

  angular
    .module('mobileApp')
    .controller('aboutCrtl', aboutCrtl);

  aboutCrtl.$inject = [];

  /* @ngInject */
  function aboutCrtl() {
    var vm = this;

    activate();

    function activate() {
      vm.active = true;
    }
  }
})();
