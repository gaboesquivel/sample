(function() {
  'use strict';
  angular.module('mobileApp')
    .config(aboutConfig);

  function aboutConfig($stateProvider) {
    $stateProvider
      .state('main.about', {
        url: '/about',
        views: {
          'pageContent': {
            templateUrl: 'views/about/about.html',
            controller: 'aboutCrtl as vm'
          }
        }
      })
      .state('main.faq', {
        url: '/faq',
        views: {
          'pageContent': {
            templateUrl: 'views/about/faq.html'
          }
        }
      })
      .state('main.legal', {
        url: '/legal',
        views: {
          'pageContent': {
            templateUrl: 'views/about/legal.html'
          }
        }
      })
      .state('main.privacy', {
        url: '/privacy',
        views: {
          'pageContent': {
            templateUrl: 'views/about/privacy.html'
          }
        }
      })
      .state('main.terms', {
        url: '/terms',
        views: {
          'pageContent': {
            templateUrl: 'views/about/terms.html'
          }
        }
      });
  }
})();
