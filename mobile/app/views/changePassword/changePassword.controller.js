(function() {
  'use strict';

  angular
    .module('mobileApp')
    .controller('ChangePasswordCtrl', ChangePasswordCtrl);

  ChangePasswordCtrl.$inject = ['$ionicLoading', 'security', 'currentUser'];

  /* @ngInject */
  function ChangePasswordCtrl($ionicLoading, security, currentUser) {
    var vm = this;
    vm.currentUserId = currentUser._id;
    vm.confirmChage = confirmChage;

    activate();

    function activate() {

    }

    function confirmChage() {
      if (vm.newPassword === vm.confirmPass) {
        vm.submitted = true;
        var data = {
          id: vm.currentUserId,
          oldPassword: vm.oldPassword,
          newPassword: vm.confirmPass
        };
        security
          .resetPassword(data)
          .then(function changeSuccess(res) {
            if (res.confirm === true) {
              vm.showMessage = res.confirm;
              vm.errPassword = false;
              vm.errCheckPassword = false;
            } else if (res === 401) {
              vm.errPassword = true;
            }
          })
          .catch(function error() {
            vm.errPassword = true;
          });
      } else {
        vm.errCheckPassword = true;
      }


    }

  }
})();
