(function() {
  'use strict';
  angular.module('mobileApp')
    .config(loginViewConfig);

  function loginViewConfig($stateProvider) {
    $stateProvider
      .state('main.changePassword', {
        url: '/changePassword',
        views: {
          'pageContent': {
            templateUrl: 'views/changePassword/changePassword.html',
            controller: 'ChangePasswordCtrl as changePasswordCtrl'
          }
        }
      });
  }
})();
