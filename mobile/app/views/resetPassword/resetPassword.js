(function() {
  'use strict';
  angular.module('mobileApp')
    .config(authConfig);

  function authConfig($stateProvider) {
    $stateProvider
      .state('public.resetPassword', {
        url: '/resetPassword',
        templateUrl: 'views/resetPassword/resetPassword.html',
        controller: 'ResetPasswordCtrl as resetPasswordCtrl'
      });
  }

})();
