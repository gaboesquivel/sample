(function() {
  'use strict';

  angular
    .module('mobileApp')
    .controller('ResetPasswordCtrl', ResetPasswordCtrl);

  ResetPasswordCtrl.$inject = ['$ionicLoading', '$cordovaDialogs', '$state', 'security'];

  /* @ngInject */
  function ResetPasswordCtrl($ionicLoading, $cordovaDialogs, $state, security) {
    var vm = this;
    vm.send = send;
    vm.showMessage = false;

    activate();

    function activate() {
      vm.active = true;
    }

    function send() {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      var check = reg.test(vm.email);
      if (check) {
        var user = {
          email: vm.email
        };
        $ionicLoading.show({
          template: '<ion-spinner class=\'spinner-energized\'></ion-spinner><br>Check...'
        });

        security
          .resetPassword(user)
          .then(function(response) {
            if (response.confirm) {
              $ionicLoading.hide();
              vm.showMessage = true;
            }
          }).catch(function err() {
            $ionicLoading.hide();
            vm.errorMessage = true;
          });
      }
    }

  }
})();
