(function() {
  'use strict';

  angular
    .module('mobileApp')
    .directive('login', login);

  /* @ngInject */
  function login() {
    var directive = {
      restrict: 'E',
      templateUrl: 'views/login/login.directive.html',
      scope: {
      },
      link: linkFunc,
      controller: LoginDirectiveCtrl,
      controllerAs: 'loginDirectiveCtrl',
      bindToController: true
    };

    return directive;

    function linkFunc() { // scope, el, attr
    }
  }

  LoginDirectiveCtrl.$inject = ['$rootScope', '$scope', '$ionicLoading', '$cordovaDialogs', '$ionicPopup', 'security', '$state'];

  /* @ngInject */
  function LoginDirectiveCtrl($rootScope, $scope, $ionicLoading, $cordovaDialogs, $ionicPopup, security, $state) {
    var vm = this;
    vm.popup;
    vm.login = login;
    vm.confirmChage = confirmChage;
    vm.displayLogin = displayLogin;
    vm.displayedLogin = false;
    vm.mapHide = true;

    activate();
    ////////////////

    function activate() {
      displayLogin();
    }

    function updateErrorMessage(msg) {
      $scope.errorMessage = msg || 'Invalid username or password';
      $scope.LoginFrm.$invalidUser = {
        fail: true
      };
    }

    function login() {
      var user = {
        email: $scope.userEmail,
        password: $scope.userPassword
      };

      if (!user.email || !user.password) {
        updateErrorMessage('Missing email or password');
        return;
      //return $cordovaDialogs.alert('Missing email or password', 'Error', 'OK');
      }

      if (/\S+@\S+\.\S+/.test(user.email) === false) {
        updateErrorMessage('Enter a valid email');
        return;
      //return $cordovaDialogs.alert('Enter a valid email', 'Error', 'OK');
      }

      $ionicLoading.show({
        template: '<ion-spinner class=\'spinner-energized\'></ion-spinner><br>Signing in...'
      });

      security.login(user)
        .then(function(response) {

          $scope.LoginFrm.$invalidUser = {};

          // verifiy response
          $log.debug('================> response', response);
          $scope.userEmail = '';
          $scope.userPassword = '';
          $state.go('main.home');

        })
        .catch(function(err) {
          $log.error('login error', err);
          if (err.data) {
            updateErrorMessage(err.data.msg);
          }
        })
        .finally(function() {
          $ionicLoading.hide();
        });
    }

    function confirmChage() {
      $log.error('login directive :: confirmChage disabled');
    // if (vm.newPassword.length > 0) {
    //   if (vm.newPassword === vm.confirmPass) {
    //     var user = {
    //       password: vm.newPassword
    //     };
    //     auth.newPassword(user).then(function(res) {
    //       if (res.confirm) {
    //         vm.state.infoUser.resetPass = false;
    //         vm.popup.close();
    //       } else {
    //         $cordovaDialogs('Error change password', 'Error');
    //       }
    //     });
    //
    //   }
    // }
    }

    /**
     * @description animate the login form
     * @return {false}
     */
    function displayLogin() {
      vm.displayedLogin = true;
      $rootScope.$broadcast('hideMap');
    }

  }
})();
