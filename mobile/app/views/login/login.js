(function() {
  'use strict';
  angular.module('mobileApp')
    .config(loginViewConfig);

  function loginViewConfig($stateProvider) {
    $stateProvider
      .state('public.login', {
        url: 'login',
        templateUrl: 'views/login/login.html',
        controller: 'LoginCtrl as loginCtrl'
      })
      .state('logout', {
        url: '/logout',
        onEnter: function logoutStateOnEnter(security) {
          security.logout();
        }
      });
  }
})();
