(function() {
  'use strict';

  angular
    .module('mobileApp')
    .controller('SignupCtrl', SignupCtrl);

  SignupCtrl.$inject = ['$scope', '$state', '$ionicLoading', 'security'];

  /* @ngInject */
  function SignupCtrl($scope, $state, $ionicLoading, security) {

    var vm = this;
    vm.user = {};
    vm.errors = {};
    vm.register = register;

    activate();

    function activate() {

    }

    function updateErrorMessage(msg) {
      $scope.errorMessage = msg || 'Please check your info';
      $scope.SignupFrm.$invalidUser = {
        fail: true
      };
    }

    function register() {

      vm.submitted = true;
      var userData = {
        name: vm.user.name,
        phone: vm.user.phone,
        email: vm.user.email,
        password: vm.user.password
      };

      $ionicLoading.show({
        template: '<ion-spinner class=\'spinner-energized\'></ion-spinner><br>Signing up...'
      });

      security
        .signup(userData)
        .then(function() {
          $state.go('main.home');
        })
        .catch(function(err) {
          //TODO: improve error handling - Gabo
          $log.error('signup error', err);
          if (err.data) {
            updateErrorMessage(err.data.msg);
          }
        })
        .finally(function() {
          $ionicLoading.hide();
        });
    }
  }
})();
