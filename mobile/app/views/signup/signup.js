(function() {
  'use strict';
  angular.module('mobileApp')
    .config(signupViewConfig);

  function signupViewConfig($stateProvider) {
    $stateProvider
      .state('public.signup', {
        url: 'signup',
        templateUrl: 'views/signup/signup.html',
        controller: 'SignupCtrl as singupCtrl'
      });
  }

})();
