(function() {
  'use strict';

  angular.module('mobileApp')
    .config(function($stateProvider) {
      $stateProvider
        .state('main.home', {
          url: '/home',
          views: {
            'pageContent': {
              templateUrl: 'views/home/home.html',
              controller: 'HomeCtrl as homeCtrl'
            }
          }
        });
    });
})();
