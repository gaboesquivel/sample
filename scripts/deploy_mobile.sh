set -e
echo 'deploying mobile app'

git config --global user.email $GIT_EMAIL
git config --global user.name $GIT_USERNAME

echo '=========================================='
echo 'upload to ionic servers'
ionic upload --email $IONIC_EMAIL --password $IONIC_PASSWORD

echo '=========================================='
echo 'push mobile app to heroku and mobile-prod branch'
cp -rf www heroku
cd heroku
git init
git remote add heroku $HEROKU_MOBILE
git add .
git commit -m 'updating mobile app from CircleCI'
echo '=========================================='
echo 'push mobile app to heroku'
git push -f heroku master

echo '=========================================='
echo 'prepare phonegap folder...'
cd ..
gulp phonegap

echo '=========================================='
echo 'push mobile app to mobile-prod branch (phonegap build)'
cd phonegap
git init
git remote add origin $GITHUB_REPO
git add .
git commit -m 'updating mobile app from CircleCI'
git checkout -b mobile-prod
git push -f origin mobile-prod
