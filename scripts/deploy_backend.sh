set -e
echo 'deploying backend app'
cd ../backend

echo '=========================================='
echo 'building backend ...'
npm run build-dev-server

echo '=========================================='
echo 'push new copy'
cd ./dist
git config --global user.email $GIT_EMAIL
git config --global user.name $GIT_USERNAME
git init
git remote add heroku $HEROKU_BACKEND
git add .
git commit -m 'updating backend website from CircleCI'
git push -f heroku master
